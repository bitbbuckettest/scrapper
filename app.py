from flask import Flask, jsonify
from flask_restful import Resource, Api
from bs4 import BeautifulSoup
import requests
import jsonpickle
from flask.json import JSONEncoder


class MyJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Product):
            return {
                'product_name': obj.name,
                'product_price': obj.price,
                'product_image': obj.image,
                'product_link': obj.link,
            }
        return super(MyJSONEncoder, self).default(obj)


app = Flask(__name__)
app.json_encoder = MyJSONEncoder
api = Api(app)

@app.route("/")
def hello():
    return "Hello World!"


class Product:

    def __init__(self,name='0',price='0',image='0',link='0'):
        """ Create a new point at the origin """
        self.name = name
        self.price = price
        self.image = image
        self.link = link

class Get(Resource):
    def get(self,product):
        session = requests.Session()
        req = session.get("https://www.pepperfry.com/site_product/search?q="+ product +"&as=0&src=os")
        req.raise_for_status()  # omit this if you dont want an exception on a non-200 response
        html = req.text
        soup = BeautifulSoup(html, 'html.parser')

        # print(soup)
        data = soup.find_all("div", {"class": "clip-crd-10x11 pf-white srch-rslt-bxwrpr"})

        product_name = []
        image_url = []
        price = []
        link = []

        for item in data:
            img_alt = item.find("img", {"class": "lazy"})
            product_name.append(img_alt.get('alt'))
            priceItem = item.find("span", {"class": "clip-offr-price "}).text.strip()
            price.append(priceItem)
            url = item.find("div", {"class": "card-img-wrp center-xs card-srch-img-wrp"}).find("img", {"alt": img_alt.get('alt')}).get('data-src')
            image_url.append(url)
            product_link = item.find("div", {"class": "card-img-wrp center-xs card-srch-img-wrp"}).find("a", {"class": None}).get('href')
            link.append(product_link)
            # product_name.append(name.get("title"))

        result = []
        for x in range(0, len(data)):
            # for x in range(0,1):
            p = Product()
            p.name = product_name[x]
            p.price = price[x]
            p.image = image_url[x]
            p.link = link[x]
            result.append(p)

        json = jsonpickle.encode(result)
        return jsonify(result)


api.add_resource(Get, '/get/<string:product>')

if __name__ == '__main__':
    app.run(debug=True)
