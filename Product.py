class Product:
    """ Point class represents and manipulates x,y coords. """

    def __init__(self,name='0',price='0'):
        """ Create a new point at the origin """
        self.name = name
        self.price = price

from bs4 import BeautifulSoup
import requests
import jsonpickle

